package com.classpath.orders.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

	private final OrderRepository orderRepository;
	private final RestTemplate restTemplate;
	
	public Order save(Order order) {
		log.info("Inside the save order method:: {}", order);
		log.info("Calling the inventory service:: ");
		ResponseEntity<Long> response =  this.restTemplate.getForEntity("http://inventory-service/api/inventory", Long.class);
		log.info("Response :: "+ response.getBody());
		return this.orderRepository.save(order);
	}
	
	public Set<Order> fetchAllOrders(){
		return new HashSet<>(this.orderRepository.findAll());
	}
	
	public Order fetchOrderById(long orderId) {
		return this.orderRepository
				.findById(orderId)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public void deleteOrderById(long id) {
		this.orderRepository.deleteById(id);
	}
}
