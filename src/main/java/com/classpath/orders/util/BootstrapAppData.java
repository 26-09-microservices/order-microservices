package com.classpath.orders.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.classpath.orders.model.LineItem;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class BootstrapAppData {
	
	private final OrderRepository orderRepository;
	private final Faker faker = new Faker();
	
	@EventListener(ApplicationReadyEvent.class)
	public void bootstrapApplication(ApplicationReadyEvent appReadyEvent) {
		log.info("Inserting orders to the table:: ");
		/*
		 * for(int i = 0; i < 10; i++){
		 * }
		 * 
		 */
		//declarative
		IntStream.range(0, 10).forEach(index -> {
			Order order = Order.builder()
							   .customerName(faker.name().firstName())
							   .orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
							   .price(faker.number().randomDouble(2, 400, 800))
							   .build();
			
			IntStream.range(0,  faker.number().numberBetween(2, 4))
					  .forEach(loop -> {
						  LineItem lineItem = LineItem.builder()
								  					.name(faker.commerce().productName())
								  					.price(faker.number().randomDouble(2, 200, 400))
								  					.qty(faker.number().numberBetween(2, 4))
								  					.build();
						  
						/*
						 * order.getLineItems().add(lineItem); lineItem.setOrder(order);
						 */
						  order.addLineItem(lineItem);
					  });
			this.orderRepository.save(order);
		});
	}

}
